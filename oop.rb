class Cat
	def hello
  	puts "hello" 
  end
end

class Cat
	def word
		puts "word"
  end
end

kitty = Cat.new
kitty.hello
kitty.word

class String
	def say_hello
		"hihihihihi #{self}"
	end
end

puts "Monkey".say_hello

class String
	def say_hello
		"Hello Hello Hello #{self}"
	end

	def length
		100
	end
end

puts "Mickey".say_hello
puts "5".length

puts 1.+(2)

class Integer
	alias :old_plus :+
	def +(n)
		"hey hey hey"
		self.old_plus(n)
	end
end

puts 1 + 3
